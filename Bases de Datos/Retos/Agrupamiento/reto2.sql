USE tienda;

# 1. ¿Cuál es el promedio de salario de los puestos?
SELECT avg (salario) FROM puesto;

# 2. ¿Cuántos artículos incluyen la palabra Pasta en su nombre?
SELECT count(nombre) FROM articulo WHERE nombre LIKE '%Pasta%';

# 3. ¿Cuál es el salario mínimo y máximo?
SELECT min(salario), max(salario) FROM  puesto;

/*
	La función max permite conocer cuantos id de puesto hay, por lo que
    al hacer max(id_puesto) se obtienen todos los id, finalmente si se hace
    una resta, se pueden conocer los últimos usuarios agregados
    
    SELECT max(id_puesto) - 5  > 995        
    
    SELECT sum(salario) FROM puesto WHERE id_puesto >= (max(id_puesto) - 5)
*/

# 4. ¿Cuál es la suma del salario de los últimos cinco puestos agregados?
SELECT sum(salario) FROM puesto WHERE id_puesto > 995;