USE tienda;


# 1
SELECT nombre FROM empleado WHERE id_puesto
IN (SELECT id_puesto FROM puesto WHERE salario < 10000);

# 2
SELECT id_empleado, min(total_ventas), max(total_ventas)
FROM (SELECT clave, id_empleado, count(*) total_ventas
FROM venta GROUP BY clave, id_empleado) AS subconsulta
GROUP BY id_empleado;

# 3
SELECT clave, id_articulo FROM venta WHERE id_articulo
IN (SELECT id_articulo FROM articulo WHERE precio > 5000);