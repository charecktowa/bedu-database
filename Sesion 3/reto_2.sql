# 1
SELECT clave, nombre, apellido_paterno FROM venta AS vent
INNER JOIN empleado AS emp ON vent.id_empleado = e.id_empleado
ORDER BY clave;

# 2

SELECT nombre FROM venta as vent 
INNER JOIN articulo as art ON vent.id_articulo = art.id_articulo
ORDER BY clave;

# 3
SELECT clave, sum(precio) AS total FROM venta AS vent 
INNER JOIN articulo as art ON vent.id_articulo = a.id_articulo
GROUP BY clave;