# 1
# creacion de la view
/*
CREATE VIEW puestos_048 AS SELECT concat(e.nombre, ' ', e.apellido_paterno), p.nombre
FROM empleado e
JOIN puesto_048 p ON e.id_puesto = p.id_puesto;
*/

SELECT * FROM puestos_048;

# 2
# creacion de la view 

/*
CREATE VIEW empleado_articulo_048 AS
SELECT v.clave, concat(e.nombre, ' ', e.apellido_paterno) nombre, a.nombre articulo
FROM venta v INNER JOIN empleado e ON v.id_empleado = e.id_empleado
INNER JOIN articulo a ON v.id_articulo = a.id_articulo
ORDER BY v.clave;
*/

SELECT * FROM empleado_articulo_048;

# 3
/*
CREATE VIEW puesto_ventas_048 AS
SELECT p.nombre, count(v.clave) total FROM venta v
INNER JOIN empleado e ON v.id_empleado = e.id_empleado
INNER JOIN puesto p ON e.id_puesto = p.id_puesto
GROUP BY p.nombre;
*/
SELECT * FROM puesto_ventas_048 ORDER BY total DESC LIMIT 1;